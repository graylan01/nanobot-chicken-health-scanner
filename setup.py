from setuptools import setup, find_packages

setup(
    name='chickenscanner',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'aiohttp',
        'aiosqlite',
        'transformers',
        'opencv-python-headless',
        'pillow',
        'llama-cpp-python'
    ],
    entry_points={
        'console_scripts': [
            'chickenscanner=chickenscanner.chickenscanner:main',
        ],
    }
)
